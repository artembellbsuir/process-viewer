#include "pch.h"

#include "Command.h"

void NoSuchCommandCommand::Execute() {
	m_writer->PrintStr(L"No such command");
}