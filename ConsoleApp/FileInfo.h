#pragma once

#include "pch.h"

class FileInfo {

public:
	static std::wstring GetFileName(HANDLE hProc);
	static std::wstring GetFileBaseName(std::wstring absoluteFilePath);
};