#include "pch.h"

#include "Command.h"
#include "ProcessInfo.h"
#include "Utils.h"

void GetProcessTimesCommand::Execute() {
	int processId = NULL;

	m_writer->PrintStr(L"Enter process id:");
	std::cin >> processId;

	HANDLE hProcess = ::OpenProcess(
		PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
		FALSE,
		processId
	);

	if (NULL == hProcess) {
		throw ::GetLastError();
	}

	auto process = new ProcessInfo(processId, hProcess);
	process->LoadTimes();
	m_writer->PrintProcessTimes(process);
	::CloseHandle(hProcess);
	delete process;
}