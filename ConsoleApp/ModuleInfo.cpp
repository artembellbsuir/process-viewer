#include "pch.h"
#include "ModuleInfo.h"

ModuleInfo::ModuleInfo(HANDLE hProc, HMODULE hMod) :
	m_baseName(std::wstring()),
	m_fileName(std::wstring()),
	m_description(std::wstring()),
	m_sizeBytes(0),
	m_loadAddr(nullptr),
	m_entrypointAddr(nullptr),
	m_fileInfo(std::map<std::wstring, std::wstring>())
{
	m_hProc = hProc;
	m_hMod = hMod;
}

bool ModuleInfo::LoadFileVersionInfo()
{	
	DWORD dwHandle;
	DWORD dwFileVersionInfoSize = ::GetFileVersionInfoSize(
		(LPCWSTR)m_fileName.c_str(),	
		&dwHandle	
	);

	if (!dwFileVersionInfoSize) {
		return false;
		throw ::GetLastError();
	}


	LPBYTE lpVersionInfo = new BYTE[dwFileVersionInfoSize];
	BOOL bFileVersionInfoRes = ::GetFileVersionInfo(
		(LPCWSTR)m_fileName.c_str(),				
		NULL,				
		dwFileVersionInfoSize,	
		lpVersionInfo			
	);

	if (!bFileVersionInfoRes) {
		throw ::GetLastError();
	}


	struct LANGANDCODEPAGE {
		WORD wLanguage;
		WORD wCodePage;
	} *lpTranslate;

	UINT cbTranslate = 0;
	BOOL bResult = ::VerQueryValue(
		lpVersionInfo,						
		TEXT("\\VarFileInfo\\Translation"),	
		(LPVOID*)&lpTranslate,				
		&cbTranslate						
	);

	if (!bResult) {
		throw 1;
	}


	UINT lpTranslateArrSize = (cbTranslate / sizeof(struct LANGANDCODEPAGE));
	LPVOID lpBuffer;
	UINT bufferSize;

	for (UINT i = 0; i < lpTranslateArrSize; i++)
	{
		std::wstringstream toHexStream;

		toHexStream << std::setfill(L'0')
			<< std::hex
			<< std::setw(4)
			<< lpTranslate[i].wLanguage;
		std::wstring wsLan = toHexStream.str();

		toHexStream.str(std::wstring());
		toHexStream << std::setw(4)
			<< lpTranslate[i].wCodePage;
		std::wstring wsCode = toHexStream.str();

		for (UINT i = 0; i < m_fileInfoFields.size(); i++)
		{
			std::wstring wsParam = m_fileInfoFields.at(i);
			std::wstring wsSubBlock = L"\\StringFileInfo\\" + wsLan + wsCode + L"\\" + wsParam;

			BOOL bQueryParamStr = ::VerQueryValue(
				lpVersionInfo,
				wsSubBlock.c_str(),
				&lpBuffer,
				&bufferSize
			);

			if (!bQueryParamStr) {
				throw 1;
			}

			std::wstring wsParamInfo((const TCHAR*)lpBuffer);
			m_fileInfo.insert(std::pair<std::wstring, std::wstring>(wsParam, wsParamInfo));
		}
	}
	return true;
}

void ModuleInfo::LoadFileMainInfo()
{
	MODULEINFO miModInfo = { 0 };
	BOOL bGetModInfoRes = ::GetModuleInformation(
		m_hProc,			
		m_hMod,				
		&miModInfo,			
		sizeof(MODULEINFO)	
	);

	if (!bGetModInfoRes) {
		throw ::GetLastError();
	}
	m_loadAddr = miModInfo.lpBaseOfDll;
	m_sizeBytes = miModInfo.SizeOfImage;
	m_entrypointAddr = miModInfo.EntryPoint;


	TCHAR szBaseName[MAX_PATH];
	DWORD dwGetModBaseName = ::GetModuleBaseName(
		m_hProc,	
		m_hMod,		
		szBaseName,	
		MAX_PATH	
	);

	if (!dwGetModBaseName) {
		throw ::GetLastError();
	}
	m_baseName = std::wstring(szBaseName);

	TCHAR szModName[MAX_PATH];
	DWORD dwStringLen = ::GetModuleFileNameEx(
		m_hProc,							
		m_hMod,								
		szModName,							
		sizeof(szModName) / sizeof(TCHAR)	
	);

	if (!dwStringLen)
	{
		throw ::GetLastError();
	}
	m_fileName = std::wstring(szModName);

}
