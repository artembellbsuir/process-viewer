#include "pch.h"

#include "Utils.h"
#include <bitset>

std::wstring Helper::GetPriorityStr(DWORD dwPriority)
{
	auto foundPosition = ProcessPriorities.find(dwPriority);
	auto endPosition = ProcessPriorities.end();

	return ProcessPriorities.at(dwPriority);
}

std::wstring Helper::GetAffinityStr(DWORD_PTR affinity)
{
	std::bitset<8> y(affinity);

	std::wstringstream buffer;
	buffer << y;

	return buffer.str();
}

std::wstring Formatter::GetSizeString(unsigned long long dwSizeBytes)
{
	std::vector<std::wstring> postfixes = { L"B", L"KB", L"MB", L"GB", L"TB" };
	double dSizeBytes = dwSizeBytes;
	DWORD i = 0;
	const unsigned int bytesInKb = 1024;

	while (dSizeBytes >= bytesInKb && postfixes.size() - 1 > i) {
		dSizeBytes /= bytesInKb;
		i++;
	}

	std::wostringstream wsStream;
	wsStream << std::fixed << std::setprecision(2) << dSizeBytes;
	wsStream << L" " << postfixes.at(i);

	return wsStream.str();
}

std::wstring Formatter::GetTrimmed(std::wstring str, unsigned int len)
{
	return str.substr(0, len);
}
