#include "pch.h"

#include "Command.h"
#include "ProcessInfo.h"
#include "Utils.h"

void GetProcessesListCommand::Execute() {
	DWORD dwProcIds[1024] = { 0 },
		dwBytesReturnedCount,
		dwProcCount,
		const dwProcIdsSize = sizeof(dwProcIds);

	BOOL bRes = ::EnumProcesses(
		dwProcIds,
		dwProcIdsSize,
		&dwBytesReturnedCount
	);

	if (!bRes) {
		throw ::GetLastError();
	}

	if (!dwBytesReturnedCount || dwProcIdsSize == dwBytesReturnedCount) {
		m_writer->PrintStr(L"Not all processes were enumerated.");
	}

	dwProcCount = dwBytesReturnedCount / sizeof(DWORD);

	m_writer->PrintProcessesHeaders();

	for (unsigned int i = 0; i < dwProcCount; i++)
	{
		if (dwProcIds[i] != 0)
		{
			HANDLE hProcess = ::OpenProcess(
				PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
				FALSE,
				dwProcIds[i]
			);

			if (NULL == hProcess)
				continue;

			auto procInfo = new ProcessInfo(dwProcIds[i], hProcess);
			procInfo->LoadMainInfo();
			procInfo->LoadModulesInfo();
			m_writer->PrintProcessMainInfo(procInfo);
			delete procInfo;
		}
	}
}