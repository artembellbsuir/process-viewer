#pragma once

#include "pch.h"

class ModuleInfo {

	HMODULE m_hMod;
	HANDLE m_hProc;


public:
	std::wstring m_baseName;
	std::wstring m_fileName;
	std::wstring m_description;

	DWORD m_sizeBytes;
	LPVOID m_loadAddr;
	LPVOID m_entrypointAddr;

	std::vector<std::wstring> m_fileInfoFields = {
		L"CompanyName",
		L"FileDescription",
		L"FileVersion",
		L"InternalName",
		L"LegalCopyright",
		L"OriginalFilename",
		L"ProductName",
		L"ProductVersion",
	};

	std::map<std::wstring, std::wstring> m_fileInfo;

	ModuleInfo(HANDLE hProc, HMODULE hMod);

	bool LoadFileVersionInfo();
	void LoadFileMainInfo();
};