#pragma once

#include <Windows.h>
#include "Writers.h"

class Command {
public:
	OutputWriter* m_writer;
	Command() : m_writer(new ConsoleWriter()) {};
	virtual void Execute() {};
};

class GetProcessesListCommand : public Command {
	void Execute();
};

class NoSuchCommandCommand : public Command {
	void Execute();
};

class GetProcessModulesCommand : public Command {
	void Execute();
};

class GetProcessMemoryInfoCommand : public Command {
	void Execute();
};

class GetProcessModulesDescriptionCommand : public Command {
	void Execute();
};

class GetProcessIOCounters : public Command {
	void Execute();
};

class GetProcessTimesCommand : public Command {
	void Execute();
};