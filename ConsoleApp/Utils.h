#pragma once

#include "pch.h"

namespace Helper {
	const std::map<DWORD, std::wstring> ProcessPriorities = {
	   {REALTIME_PRIORITY_CLASS, L"Realtime"},
	   {HIGH_PRIORITY_CLASS, L"High"},
	   {ABOVE_NORMAL_PRIORITY_CLASS, L"Above normal"},
	   {NORMAL_PRIORITY_CLASS, L"Normal"},
	   {BELOW_NORMAL_PRIORITY_CLASS, L"Below normal"},
	   {IDLE_PRIORITY_CLASS, L"Idle"},
	};
	std::wstring GetPriorityStr(DWORD dwPriority);
	std::wstring GetAffinityStr(DWORD_PTR affinity);
}


namespace Formatter {
	std::wstring GetSizeString(unsigned long long dwSize);
	std::wstring GetTrimmed(std::wstring str, unsigned int len);
}