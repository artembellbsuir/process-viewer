#include "pch.h"
#include "ProcessInfo.h"
#include "Utils.h"

ProcessInfo::ProcessInfo(DWORD id, HANDLE hProc) : 
	m_hProc(hProc), 
	m_id(id),
	m_modulesNumber(0)
{
	//m_hProc = hProc;
}

void ProcessInfo::GetPriority(HANDLE hProc)
{
	DWORD dwPriority = ::GetPriorityClass(hProc);
	if (!dwPriority) {
		DWORD dwLastErr = ::GetLastError();
		//return dwLastErr;
	}

	m_priority = dwPriority;
}

void ProcessInfo::GetAffinityMask(HANDLE hProc)
{

	DWORD_PTR lpProcessAffinityMask,
		lpSystemAffinityMask;
	DWORD dwGetPriorityMaskRes = ::GetProcessAffinityMask(
		hProc,
		&lpProcessAffinityMask,
		&lpSystemAffinityMask
	);

	if (!dwGetPriorityMaskRes) {
		
	}

	m_affinityMask = lpProcessAffinityMask;
}

PROCESS_MEMORY_COUNTERS ProcessInfo::LoadMemoryInfo()
{
	BOOL bProcMemInfoRes = ::GetProcessMemoryInfo(
		m_hProc,
		&m_pmc,
		sizeof(m_pmc)
	);

	if (!bProcMemInfoRes)
	{
		auto errCode = ::GetLastError();
	}

	return m_pmc;
}

void ProcessInfo::LoadModulesInfo()
{
	DWORD cbBytesForModules;
	BOOL bRes = ::EnumProcessModules(
		m_hProc,
		m_hMods,
		sizeof(m_hMods),
		&cbBytesForModules
	);


	if (bRes){
		m_modulesNumber = cbBytesForModules / sizeof(HMODULE);
	}

}

void ProcessInfo::LoadIOInfo()
{
	BOOL bGetProcIORes = ::GetProcessIoCounters(
		m_hProc,
		&m_ioCounters
	);

	if (!bGetProcIORes)
	{
		DWORD dwLastErr = ::GetLastError();
		throw dwLastErr;
	}
}

void ProcessInfo::LoadTimes()
{
	
}

void ProcessInfo::LoadMainInfo()
{
	m_fileName = FileInfo::GetFileName(m_hProc);
	m_baseName = FileInfo::GetFileBaseName(m_fileName);
	GetPriority(m_hProc);
	GetAffinityMask(m_hProc);
}
