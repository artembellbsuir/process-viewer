#pragma once

#include "pch.h"

#include "ProcessInfo.h"
#include "ModuleInfo.h"

class OutputWriter {
public:
	void virtual PrintProcessesHeaders() {} 
	void virtual PrintStr(std::wstring str) {}
	void virtual PrintProcessMainInfo(ProcessInfo* processInfo) {}
	void virtual PrintProcessMemoryInfo(ProcessInfo* processInfo) {};
	void virtual PrintProcessIOInfo(ProcessInfo* processInfo) {}
	void virtual PrintProcessTimes(ProcessInfo* processInfo) {}

	void virtual PrintModulesHeaders() {};
	void virtual PrintModuleMainInfo(ModuleInfo* modInfo) {};
	void virtual PrintModuleFileVersionInfo(ModuleInfo* modInfo) {};
};

class ConsoleWriter : public OutputWriter  {
public:
	void PrintProcessesHeaders();
	void PrintStr(std::wstring str);
	void PrintProcessMainInfo(ProcessInfo* processInfo);
	void PrintProcessMemoryInfo(ProcessInfo* processInfo);
	void PrintProcessIOInfo(ProcessInfo* processInfo);
	void PrintProcessTimes(ProcessInfo* processInfo);

	void PrintModulesHeaders();
	void PrintModuleMainInfo(ModuleInfo* modInfo);
	void PrintModuleFileVersionInfo(ModuleInfo* modInfo);
};