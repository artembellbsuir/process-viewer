#include "pch.h"

#pragma comment(lib, "Psapi.lib")
// and compile with -DPSAPI_VERSION=1

#include "Command.h"
#include "ProcessInfo.h"
#include "ModuleInfo.h"
#include "Utils.h"

void GetProcessModulesCommand::Execute() {
	int processId = NULL;

	m_writer->PrintStr(L"Enter process id:");
	std::cin >> processId;

	HANDLE hProcess = ::OpenProcess(
		PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
		FALSE,
		processId
	);

	if (NULL == hProcess) {
		throw::GetLastError();
	}

	auto process = new ProcessInfo(processId, hProcess);
	process->LoadModulesInfo();

	m_writer->PrintModulesHeaders();

	for (unsigned int i = 0; i < process->m_modulesNumber; i++)
	{
		auto modInfo = new ModuleInfo(hProcess, process->m_hMods[i]);
		modInfo->LoadFileMainInfo();
		m_writer->PrintModuleMainInfo(modInfo);
		delete modInfo;
	}

	::CloseHandle(hProcess);
	delete process;
}

