#include "pch.h"

#pragma comment(lib, "Psapi.lib")

#include "Command.h"
#include "ProcessInfo.h"
#include "Utils.h"
#include "ModuleInfo.h"

void GetProcessModulesDescriptionCommand::Execute() {
	int processId = NULL;

	m_writer->PrintStr(L"Enter process id:");
	std::cin >> processId;

	HANDLE hProcess = ::OpenProcess(
		PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
		FALSE,
		processId
	);

	if (NULL == hProcess) {
		throw::GetLastError();
	}

	auto process = new ProcessInfo(processId, hProcess);
	process->LoadModulesInfo();

	for (unsigned int i = 0; i < process->m_modulesNumber; i++)
	{
		auto modInfo = new ModuleInfo(hProcess, process->m_hMods[i]);
		modInfo->LoadFileMainInfo();
		if (modInfo->LoadFileVersionInfo()) {
			m_writer->PrintModuleFileVersionInfo(modInfo);
		}
		else {
			m_writer->PrintStr(L"Cannot fetch info of this module");
		}

		

		delete modInfo;
	}

	::CloseHandle(hProcess);
	delete process;
}