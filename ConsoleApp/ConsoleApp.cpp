﻿#include "pch.h"
#pragma comment(lib, "Version.lib")

#include "ModuleInfo.h"
#include "ProcessInfo.h"
#include "Command.h"
#include "CommandProvider.h"

int main(void)
{
	setlocale(LC_ALL, "Russian");

	CommandProvider commandProvider;
	Command *command = nullptr;
	std::string commandName;
	
	std::cout << "Enter any command..." << std::endl;
	do {
		commandName.clear();
		if (std::getline(std::cin, commandName)) {
			command = commandProvider.GetCommand(commandName);
			command->Execute();
		}
	} while (commandName != COMMAND_EXIT);

	return 0;
}
