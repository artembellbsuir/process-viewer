#pragma once

#include "pch.h"

#include "FileInfo.h"

#define MAX_MODULES_NUMBER 1000

class ProcessInfo : FileInfo {
	HANDLE m_hProc;

public:
	std::wstring m_baseName;
	std::wstring m_fileName;
	std::wstring m_description;
	std::wstring m_owner;

	DWORD m_id;
	DWORD m_priority;
	DWORD_PTR m_affinityMask;
	PROCESS_MEMORY_COUNTERS m_pmc;
	IO_COUNTERS m_ioCounters;

	DWORD m_sizeBytes;
	LPVOID m_loadAddr;
	LPVOID m_entrypointAddr;

	std::map<std::wstring, std::wstring> m_fileInfo;

	FILETIME m_creationTime;
	FILETIME m_exitTime;
	FILETIME m_kernelTime;
	FILETIME m_userTime;

	// modules
	HMODULE m_hMods[1000];
	DWORD m_modulesNumber;



	ProcessInfo(DWORD id, HANDLE hProc);

	void GetPriority(HANDLE hProc);
	void GetAffinityMask(HANDLE hProc);
	

	PROCESS_MEMORY_COUNTERS LoadMemoryInfo();

	void LoadModulesInfo();

	void LoadIOInfo();
	void LoadTimes();
	void LoadMainInfo();
};