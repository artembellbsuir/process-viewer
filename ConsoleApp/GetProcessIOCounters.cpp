#include "pch.h"

#include "Command.h"
#include "ProcessInfo.h"
#include "Utils.h"

void GetProcessIOCounters::Execute() {
	int processId = NULL;

	m_writer->PrintStr(L"Enter process id:");
	std::cin >> processId;

	HANDLE hProcess = ::OpenProcess(
		PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
		FALSE,
		processId
	);

	if (NULL == hProcess) {
		throw ::GetLastError();
	}

	auto process = new ProcessInfo(processId, hProcess);
	process->LoadIOInfo();
	m_writer->PrintProcessIOInfo(process);
	::CloseHandle(hProcess);
	delete process;
}