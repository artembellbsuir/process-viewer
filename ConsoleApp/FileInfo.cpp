#include "pch.h"
#include "FileInfo.h"

std::wstring FileInfo::GetFileName(HANDLE hProc)
{
	TCHAR szBaseName[MAX_PATH];
	DWORD dwStringLen = ::GetModuleFileNameEx(
		hProc,							
		NULL,								
		szBaseName,							
		sizeof(szBaseName) / sizeof(TCHAR)	
	);

	if (!dwStringLen) {
		DWORD dwLastErr = ::GetLastError();
		return L"UNKNOWN";
	}

	std::wstring wsBaseName(szBaseName);

	return wsBaseName;
}

std::wstring FileInfo::GetFileBaseName(std::wstring absoluteFilePath)
{
	return absoluteFilePath.substr(absoluteFilePath.rfind(L"\\") + 1);
}
