#include "pch.h"

#include "Writers.h"
#include "ProcessInfo.h"
#include "Utils.h"

void ConsoleWriter::PrintProcessesHeaders()
{
	std::string pidStr = "PID",
		nameStr = "Name",
		affinityStr = "Affinity",
		priorityStr = "Priority",
		modulesStr = "Modules count";

	std::cout <<
		std::setw(6) << pidStr << " | " <<
		std::setw(30) << nameStr << " | " <<
		std::setw(8) << affinityStr << " | " <<
		std::setw(12) << priorityStr << " | " <<
		std::setw(modulesStr.length()) << modulesStr << " | " <<
		std::endl;
}

void ConsoleWriter::PrintStr(std::wstring str)
{
	std::wcout << str << std::endl;
}

void ConsoleWriter::PrintProcessMainInfo(ProcessInfo* processInfo) {
	std::wcout <<
		std::setw(6) << processInfo->m_id << " | " <<
		std::setw(30) << Formatter::GetTrimmed(processInfo->m_baseName, 30) << " | " <<
		std::setw(8) << Helper::GetAffinityStr(processInfo->m_affinityMask) << " | " <<
		std::setw(12) << Helper::GetPriorityStr(processInfo->m_priority) << " | " <<
		std::setw(13) << processInfo->m_modulesNumber << " | " <<
		std::endl;
}

void ConsoleWriter::PrintProcessMemoryInfo(ProcessInfo* processInfo)
{
	std::wcout <<
		std::setw(33) << "Page Fault Count:" << std::setw(20) << processInfo->m_pmc.PageFaultCount << std::endl <<
		std::setw(33) << "Peak Working Set Size:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.PeakWorkingSetSize) << std::endl <<
		std::setw(33) << "Working Set Size:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.WorkingSetSize) << std::endl <<
		std::setw(33) << "Quota Peak Paged Pool Usage:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.QuotaPeakPagedPoolUsage) << std::endl <<
		std::setw(33) << "Quota Paged Pool Usage:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.QuotaPagedPoolUsage) << std::endl <<
		std::setw(33) << "Quota Peak Non Paged Pool Usage:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.QuotaPeakNonPagedPoolUsage) << std::endl <<
		std::setw(33) << "Quota Non Paged Pool Usage:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.QuotaNonPagedPoolUsage) << std::endl <<
		std::setw(33) << "Pagefile Usage:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.PagefileUsage) << std::endl <<
		std::setw(33) << "Peak Pagefile Usage:" << std::setw(20) << Formatter::GetSizeString(processInfo->m_pmc.PeakPagefileUsage) << std::endl;
}

void ConsoleWriter::PrintProcessIOInfo(ProcessInfo* processInfo)
{
	std::wcout <<
		std::setw(23) << "Read Operation Count:" << std::setw(20) << processInfo->m_ioCounters.ReadOperationCount << std::endl <<
		std::setw(23) << "Read Transfer Count:" << std::setw(20) << processInfo->m_ioCounters.ReadTransferCount << std::endl <<
		std::setw(23) << "Write Operation Count:" << std::setw(20) << processInfo->m_ioCounters.WriteOperationCount << std::endl <<
		std::setw(23) << "Write Transfer Count:" << std::setw(20) << processInfo->m_ioCounters.WriteTransferCount << std::endl <<
		std::setw(23) << "Other Operation Count:" << std::setw(20) << processInfo->m_ioCounters.OtherOperationCount << std::endl <<
		std::setw(23) << "Other Transfer Count:" << std::setw(20) << processInfo->m_ioCounters.OtherTransferCount << std::endl << std::endl;
}

void ConsoleWriter::PrintProcessTimes(ProcessInfo* processInfo)
{

}

void ConsoleWriter::PrintModuleFileVersionInfo(ModuleInfo* modInfo)
{
	std::wcout <<
		std::setw(20) << "Company Name:" << std::setw(80) << modInfo->m_fileInfo.at(L"CompanyName") << std::endl <<
		std::setw(20) << "File Description:" << std::setw(80) << modInfo->m_fileInfo.at(L"FileDescription") << std::endl <<
		std::setw(20) << "File Version:" << std::setw(80) << modInfo->m_fileInfo.at(L"FileVersion") << std::endl <<
		std::setw(20) << "Internal Name:" << std::setw(80) << modInfo->m_fileInfo.at(L"InternalName") << std::endl <<
		std::setw(20) << "Legal Copyright:" << std::setw(80) << modInfo->m_fileInfo.at(L"LegalCopyright") << std::endl <<
		std::setw(20) << "Original Filename:" << std::setw(80) << modInfo->m_fileInfo.at(L"OriginalFilename") << std::endl <<
		std::setw(20) << "Product Name:" << std::setw(80) << modInfo->m_fileInfo.at(L"ProductName") << std::endl <<
		std::setw(20) << "Product Version:" << std::setw(80) << modInfo->m_fileInfo.at(L"ProductVersion") << std::endl << std::endl;
}

void ConsoleWriter::PrintModulesHeaders()
{
	std::wcout <<
		std::setw(30) << "Name" << " | " <<
		std::setw(15) << "Load address" << " | " <<
		std::setw(20) << "Entrypoint address" << " | " <<
		std::setw(10) << "Size" << std::endl;
}

void ConsoleWriter::PrintModuleMainInfo(ModuleInfo* modInfo)
{
	std::wcout <<
		std::setw(30) << modInfo->m_baseName << " | " <<
		std::setw(15) << std::hex << modInfo->m_loadAddr << " | " <<
		std::setw(20) << std::hex << modInfo->m_entrypointAddr << " | " <<
		std::setw(10) << std::dec << Formatter::GetSizeString(modInfo->m_sizeBytes) << std::endl;
}
