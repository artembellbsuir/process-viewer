#pragma once

#include "pch.h"

#include "Command.h"

constexpr auto COMMAND_GET_PROCESSES_LIST = "list";
constexpr auto COMMAND_EXIT = "exit";
constexpr auto COMMAND_GET_PROCESS_MODULES = "modules";
constexpr auto COMMAND_GET_PROCESS_MEM_INFO = "memory";
constexpr auto COMMAND_GET_PROCESS_MODULES_DESCRIPTION = "modules descriptions";
constexpr auto COMMAND_GET_PROCESS_IO_COUNTERS = "io";
constexpr auto COMMAND_GET_PROCESS_TIMES = "times";

class CommandProvider {
private:
	std::map<std::string, Command*> m_commands;

public:
	CommandProvider() {
		m_commands.insert({ COMMAND_GET_PROCESSES_LIST, new GetProcessesListCommand() });
		m_commands.insert({ COMMAND_GET_PROCESS_MODULES, new GetProcessModulesCommand() });
		m_commands.insert({ COMMAND_GET_PROCESS_MEM_INFO, new GetProcessMemoryInfoCommand() });
		m_commands.insert({ COMMAND_GET_PROCESS_MODULES_DESCRIPTION , new GetProcessModulesDescriptionCommand() });
		m_commands.insert({ COMMAND_GET_PROCESS_IO_COUNTERS, new GetProcessIOCounters() });
		m_commands.insert({ COMMAND_GET_PROCESS_TIMES , new GetProcessTimesCommand() });
	}

	Command* GetCommand(std::string commandName) {
		if (m_commands.find(commandName) != m_commands.end()) {
			return m_commands.at(commandName);
		}
		return new NoSuchCommandCommand();
	}
};